# Simulate a 3000ms acoustic delay
# @SimACDly=3000
# 800 bps
@TxRate=5
@TxPower=8
@LocalAddr=2
@IdleTimer=00:20:00
@FwdDelay=0.5
@DevEnable=0
@Verbose=0
# Default to on-line mode
@OpMode=Online
+++
cfg store
