#!/usr/bin/env bash

set -e
vers=$(git describe --tags --always --dirty --match=v* 2> /dev/null || \
	   cat $(pwd)/.version 2> /dev/null || echo v0)
VERSION="${vers#v}"

export VERSION
/usr/local/bin/nfpm pkg --target abprsys_${VERSION}_any.deb
